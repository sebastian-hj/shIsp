classdef PlotUtilities
    % PlotUtilities Plot utilities
    
    properties (Access = private)
        cfaPattern;
    end
    
    methods (Access = public)
        function obj = PlotUtilities(rawInfo)
            % PlotUtilities Construct an instance of this class
            obj.cfaPattern = rawInfo.cfaPattern;
        end
    end


    methods (Access = public, Static = true)
        function plotRaw(raw, plotTitle)
            % plotRaw Plot raw image

            % Start figure
            figure;
            % Plot image scaled
            imagesc(raw);
            % Set title
            title(plotTitle);
            
            % Set axis as image
            axis image;
            % Set colormap as grayscale
            colormap(gray);
            % Display colorbar
            colorbar;
        end
        
        
        function plotColorChannels(colorChannels, plotTitle)
            % plotColorChannels Plot color channels
            
            % Start figure
            figure;
            
            % Add title
            sgtitle(plotTitle);
            
            % Add subplots
            subplot(2, 2, 1);
            imagesc(colorChannels.r);
            title("R");
            axis image;
            colorbar;
            
            subplot(2, 2, 2);
            imagesc(colorChannels.gr);
            title("Gr");
            axis image;
            colorbar;
            
            subplot(2, 2, 3);
            imagesc(colorChannels.gb);
            title("Gb");
            axis image;
            colorbar;
            
            subplot(2, 2, 4);
            imagesc(colorChannels.b);
            title("B");
            axis image;
            colorbar;
            
            % Set colormap as grayscale
            colormap(gray);
        end
    end
end

