classdef (Sealed) CfaPattern
    %CFAPATTERN CFA Pattern Class
    
    enumeration
        rggb, grbg, gbrg, bggr
    end
end

